#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <utilfecha.h>

int argumento;
int main(int argc, char **argv){

	char opcion;
	while( (opcion = getopt(argc, argv, "s:d:")) != -1 ){
		switch(opcion){
			case 's':
				argumento = atoi(optarg);
				segundos(argumento);
				break;
			case 'd':
				argumento = atoi(optarg);
				dias(argumento);
				break;
			default:
				break;
		}
	}
	return 0;
}
