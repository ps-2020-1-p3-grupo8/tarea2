#include <stdio.h>

void dias(int days){
	float anios = days/360;
	
	int anios_redondeado= anios;
	
	float meses = (days-(anios_redondeado*360))/30;
	
	int meses_redondeados= meses;
	
	float dias = (days)-(anios_redondeado*360)-(meses_redondeados*30);
	
	printf("años\t\tmeses\t\tdias\n");
  	printf("%.0f\t\t%.0f\t\t%.0f\n",anios,meses,dias);
}
