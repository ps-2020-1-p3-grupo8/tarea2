CC=gcc -Wall -c 
bin/utilfecha: obj/segundos.o obj/dias.o obj/main.o
	gcc obj/segundos.o obj/dias.o obj/main.o -I ./include -o bin/utilfecha

obj/segundos.o: src/segundos.c
	$(CC) -I include/ src/segundos.c -o obj/segundos.o

obj/dias.o: src/dias.c
	$(CC)-I include/ src/dias.c -o obj/dias.o
	
obj/main.o : src/main.c
	$(CC) -I include/ src/main.c -o obj/main.o 


clean:
	rm obj/*.o bin/utilfecha

